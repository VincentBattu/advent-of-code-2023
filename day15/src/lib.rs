#[derive(Debug)]
struct Lens<'a> {
    label: &'a str,
    focal_length: u32,
}

pub fn part1(input: &str) -> usize {
    input.split(',').map(hash).sum()
}

pub fn part2(input: &str) -> usize {
    let mut boxes: Vec<Vec<Lens>> = Vec::with_capacity(256);

    (0..boxes.capacity()).for_each(|_i| {
        boxes.push(Vec::new());
    });

    input.split(',').for_each(|step| {
        if step.ends_with('-') {
            let label = &step[0..step.len() - 1];
            let index: usize = hash(label);
            let index_in_box = boxes[index].iter().enumerate().find_map(
                |(i, lens)| {
                    if label == lens.label {
                        Some(i)
                    } else {
                        None
                    }
                });
            if let Some(index_in_box) = index_in_box {
                boxes[index].remove(index_in_box);
            }
        } else {
            let (label, focal_length) = step.split_once('=').unwrap();
            let index: usize = hash(label);
    
            let focal_length = focal_length.parse::<u32>().unwrap();

            let old_lens = boxes[index]
                .iter_mut()
                .find(|len| len.label == label);

            if let Some(old_lens) = old_lens {
                old_lens.focal_length = focal_length;
            } else {
                boxes[index].push(Lens {
                    focal_length,
                    label
                });
            }

        }
    });

    boxes
        .into_iter()
        .enumerate()
        .map(|(box_number, lens_box)| {
            lens_box
                .into_iter()
                .enumerate()
                .map(|(slot, lens)| {
                    (box_number + 1) * (slot + 1) * lens.focal_length as usize
                })
                .sum::<usize>()
        })
        .sum()
}

fn hash(str: &str) -> usize {
    str.chars().fold(0, |mut hash, char| {
        hash += char as usize;
        hash *= 17;
        hash %= 256;
        hash
    })
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn test_part1() {
        let input = include_str!("../../input/examples/15.txt");

        assert_eq!(part1(input), 1_320);
    }

    #[test]
    fn test_part2() {
        let input = include_str!("../../input/examples/15.txt");

        assert_eq!(part2(input), 145);
    }
}
