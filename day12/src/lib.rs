use std::collections::HashMap;

pub fn part1(input: &str) -> u64 {
    input
    .lines()
    .map(|line| {
        let (pattern, groups) = line.split_once(' ').unwrap();
        let groups = groups
            .split(',')
            .map(|number| number.parse::<u32>().unwrap())
            .collect::<Vec<_>>();

            let mut cache = HashMap::new();

            count_combinations(pattern, &groups, &mut cache)
    })
    .sum()
}

fn count_combinations (pattern: &str, groups: &[u32], cache: &mut HashMap<String, u64>) -> u64 {
    let key_cache = key_cache(pattern, groups);

    if cache.contains_key(&key_cache) {
        return *cache.get(&key_cache).unwrap();
    }

    let trim_pattern = pattern.trim_start_matches('.');

    if trim_pattern.is_empty() {
        return if groups.is_empty() { 1 } else { 0 };
    }

    if groups.is_empty() {
        return if !trim_pattern.contains('#') { 1 } else { 0 }; 
    }

    if trim_pattern.starts_with('#') {
        let current_group = groups[0] as usize;
        // pattern too short to contain the group
        if trim_pattern.len()< current_group {
            return 0;
        }

        // current pattern too short
        if trim_pattern[..current_group].contains('.') {
            return 0;
        }

        if groups.len() == 1 && trim_pattern.len() > current_group && &trim_pattern[current_group..current_group + 1] == "#" {
            return 0;
        }


        if trim_pattern.len() > current_group + 1 {
            let character_after_group = &trim_pattern[current_group..current_group + 1];
            if character_after_group == "#" {
                return 0;
            }
        }

        if trim_pattern.len() == current_group && !trim_pattern.contains('.') {
            return if groups.len() == 1 { 1 } else { 0 }
        }

 
        let res =  count_combinations(&trim_pattern[current_group + 1..], &groups[1..], cache);
        cache.insert(key_cache, res);
        return res;
    }

    let remaining_pattern = &trim_pattern[1..];
    let res: u64 = count_combinations(&format!("#{}", remaining_pattern), groups, cache) // replace "?" by "#"
        + count_combinations(remaining_pattern, groups, cache); // replace "?" by "." (simply remove it since we trim ".")
    cache.insert(key_cache, res);
    res
}

fn key_cache (pattern: & str, groups: &[u32]) -> String {
    format!("{pattern}-{}", groups
        .iter()
        .map(|group| group.to_string())
        .collect::<Vec<_>>()
        .join("-"))
}

pub fn part2(input: &str) -> u64 {
    input
    .lines()
    .map(|line| {
        let (pattern, groups) = line.split_once(' ').unwrap();

        let pattern = format!("{pattern}?{pattern}?{pattern}?{pattern}?{pattern}");
        let groups = format!("{groups},{groups},{groups},{groups},{groups}");


        let groups = groups
            .split(',')
            .map(|number| number.parse::<u32>().unwrap())
            .collect::<Vec<_>>();

            let mut cache = HashMap::new();

            count_combinations(&pattern, &groups, &mut cache)
    })
    .sum()
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1("???.### 1,1,3"), 1);
        assert_eq!(part1(".??..??...?##. 1,1,3"), 4);
        assert_eq!(part1("?#?#?#?#?#?#?#? 1,3,1,6"), 1);
        assert_eq!(part1("????.#...#... 4,1,1"), 1);
        assert_eq!(part1("????.######..#####. 1,6,5"), 4);
        assert_eq!(part1("?###???????? 3,2,1"), 10);

        assert_eq!(part1("???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1"), 21);


    assert_eq!(part1("#??# 3"), 0);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2("???.### 1,1,3"), 1);
        assert_eq!(part2(".??..??...?##. 1,1,3"), 16384);

        assert_eq!(part2("???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1"), 525152)
    }
}
