pub fn part1(input: &str) -> u64 {
    let lines = input
        .lines()
        .collect::<Vec<_>>();

    let times = parse_line(lines[0]);
    let distances = parse_line(lines[1]);

    times
        .into_iter()
        .zip(distances)
        .map(|(race_duration, record)| {
            (1..race_duration)
                .filter(|hold_time| {
                    let speed = hold_time;
                    let total_distance = speed * (race_duration - hold_time);
                    total_distance > record
                }).count() as u64
        })
        .product()

}

fn parse_line (line: &str) -> Vec<u64> {
    line
        .split_once(':')
        .unwrap()
        .1
        .split(' ')
        .filter(|line| !line.is_empty())
        .map(|number| number.parse::<u64>().unwrap())
        .collect::<Vec<_>>()
}

pub fn part2(input: &str) -> u64 {
    part1(&input.replace(' ', ""))
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn test_part1() {
        let input = include_str!("../../input/examples/6.txt");

        assert_eq!(part1(input), 288);
    }

    #[test]
    fn test_part2() {
        let input = include_str!("../../input/examples/6.txt");

        assert_eq!(part2(input), 71503);
    }
}
