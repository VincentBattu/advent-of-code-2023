
pub fn part1 (input: &str) -> u64 {
    solve(input, 1)
}

pub fn part2 (input: &str) -> u64 {
    solve(input, 1_000_000)
}

fn solve (input: &str, expansion: usize) -> u64 {
    let map = parse(input);
    let mut galaxy_positions = get_galaxy_positions(&map);
    expand(&map, &mut galaxy_positions, (expansion - 1).max(1));
    let pairs = combinations(&galaxy_positions);

    pairs
        .iter()
        .fold(0, |sum, pair| {
            let (el1, el2) = pair;

            sum + distance(*el1, *el2)
        })
}

fn parse (input: &str) -> Vec<Vec<char>> {
    input
        .lines()
        .map(|line| {
            line
                .chars()
                .collect()
        })
        .collect()
}

fn expand (map: &Vec<Vec<char>>, galaxy_positions: &mut [(usize, usize)], expansion: usize) {
    expand_rows(map, galaxy_positions, expansion);
    expand_cols(map, galaxy_positions, expansion);
}

fn expand_rows (map: &Vec<Vec<char>>, galaxy_positions: &mut [(usize, usize)], expansion: usize) {
    for y in (0..map.len()).rev() {
        let no_galaxy = map[y]
            .iter()
            .all(|char| *char != '#');

        if no_galaxy {
            galaxy_positions
                .iter_mut()
                .filter(|(_, galaxy_y)|  *galaxy_y > y)
                .for_each(|(_, galaxy_y)| *galaxy_y += expansion)
        }
    }
}

fn expand_cols (map: &[Vec<char>], galaxy_positions: &mut [(usize, usize)], expansion: usize) {
    for x in (0..map[0].len()).rev() {
        let no_galaxy = map
            .iter()
            .all(|line| line[x] != '#');

        if no_galaxy {
            galaxy_positions
                .iter_mut()
                .filter(|(galaxy_x, _)|  *galaxy_x > x)
                .for_each(|(galaxy_x, _)| *galaxy_x += expansion)
        }
    }
}

fn get_galaxy_positions (map: &[Vec<char>]) -> Vec<(usize, usize)> {
    map
        .iter()
        .enumerate()
        .flat_map(|(y, line)| {
            line
                .iter()
                .enumerate()
                .filter_map(move |(x, char)| {
                    if *char == '#' {
                        Some((x, y))
                    } else {
                        None
                    }
                })
        })
        .collect()
}

fn combinations (positions: &[(usize, usize)]) -> Vec<((usize, usize), (usize, usize))> {
    positions
        .iter()
        .enumerate()
        .flat_map(|(i, el1)| {
            positions[i + 1..]
                .iter()
                .enumerate()
                .map(move |(_, el2)| (*el1, *el2))
        })
        .collect()
}

fn distance (first: (usize, usize), second: (usize, usize)) -> u64 {
    ((first.0 as i64 - second.0 as i64).abs() + (first.1 as i64 - second.1 as i64).abs()) as u64
}

#[cfg(test)]
mod test {
    use crate::*;


    #[test]
    fn test1() {
        let input = "...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....";

    assert_eq!(part1(input), 374);
    }

    #[test]
    fn test2() {
        let input = "...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....";

    assert_eq!(solve(input, 10), 1030);
    assert_eq!(solve(input, 100), 8410);
    }
}