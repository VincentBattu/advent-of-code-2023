use std::collections::BinaryHeap;

pub fn part1(input: &str) -> u32 {
    let map = input
        .lines()
        .map(|line| line.chars().map(|char| char.to_digit(10).unwrap()).collect::<Vec<_>>())
        .collect::<Vec<_>>();

    dbg!(map);

    find_min_heat_loss(&map)
}

pub fn part2(input: &str) -> u32 {
    0
}

fn find_min_heat_loss(map: &[Vec<u32>]) -> u32 {
    let mut to_visit = BinaryHeap::new();

    while let Some(position) = to_visit.pop(){
        
    }

    0
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn test_part1() {
        let input = include_str!("../../input/examples/17.txt");

        assert_eq!(part1(input), 0);
    }

    #[test]
    fn test_part2() {
        let input = include_str!("../../input/examples/17.txt");

        assert_eq!(part2(input), 0);
    }
}
