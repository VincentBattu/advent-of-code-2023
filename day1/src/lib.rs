pub fn part1(input: &str) -> u32 {
    input.lines()
        .map(|line| { get_line_value(line) })
        .sum()
}

pub fn part2(input: &str) -> u32 {
    input.lines()
        .map(|line| {
            let line = line
                .replace("one", "one1one")
                .replace("two", "two2two")
                .replace("three", "three3three")
                .replace("four", "four4four")
                .replace("five", "five5five")
                .replace("six", "six6six")
                .replace("seven", "seven7seven")
                .replace("eight", "eight8eight")
                .replace("nine", "nine9nine");
            get_line_value(&line)
        })
        .sum()
}


fn get_line_value (line: &str) -> u32 {
    let digits = line
        .chars()
        .filter(|char| char.is_ascii_digit())
        .map(|c| c.to_digit(10).unwrap())
        .collect::<Vec<_>>();

    digits.first().unwrap() * 10 + digits.last().unwrap()
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn test_part1() {
        let input = include_str!("../../input/examples/1-part1.txt");

        assert_eq!(part1(input), 142);
    }

    #[test]
    fn test_part2() {
        let input = include_str!("../../input/examples/1-part2.txt");

        assert_eq!(part2(input), 281);
    }
}
