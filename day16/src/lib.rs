use std::collections::{HashSet, VecDeque};

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
enum Direction {
    Left,
    Right,
    Top,
    Bottom,
}

impl Direction {
    fn direction_offset(&self) -> (i32, i32) {
        match self {
            Direction::Left => (-1, 0),
            Direction::Right => (1, 0),
            Direction::Top => (0, -1),
            Direction::Bottom => (0, 1),
        }
    }
}

#[derive(PartialEq, Eq, Hash, Clone)]
struct Position {
    y: i32,
    x: i32,
}

impl Position {
    fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }

    fn add(&mut self, x: i32, y: i32) {
        self.x += x;
        self.y += y;
    }
}

struct Map {
    data: Vec<Vec<char>>,
    height: i32,
    width: i32,
}

impl Map {
    fn from(input: &str) -> Self {
        let data = input
            .lines()
            .map(|line| line.chars().collect::<Vec<_>>())
            .collect::<Vec<_>>();

        let height = data.len() as i32;
        let width = data[0].len() as i32;

        Self {
            data,
            height,
            width,
        }
    }

    fn get(&self, position: &Position) -> char {
        self.data[position.y as usize][position.x as usize]
    }

    fn is_valid_position(&self, position: &Position) -> bool {
        position.x >= 0 && position.x < self.width && position.y >= 0 && position.y < self.height
    }
}

pub fn part1(input: &str) -> usize {
    let map = Map::from(input);

    count_energized_tiles(&map, Position::new(0, 0), Direction::Right)
}

pub fn part2(input: &str) -> usize {
    let map = Map::from(input);

    let mut max = 0;
    for x in 0..map.width {
        max = max.max(count_energized_tiles(&map, Position::new(x, 0), Direction::Bottom));
        max = max.max(count_energized_tiles(&map, Position::new(x, map.height - 1), Direction::Top));
    }

    for y in 0..map.height {
        max = max.max(count_energized_tiles(&map, Position::new(0, y), Direction::Bottom));
        max = max.max(count_energized_tiles(&map, Position::new(map.width - 1, y), Direction::Bottom));
    }

    max
}

fn count_energized_tiles (map: &Map, start_position: Position, start_direction: Direction) -> usize {
    let mut visited = HashSet::<(Position, Direction)>::new();
    let mut queue = VecDeque::<(Position, Direction)>::new();
    queue.push_back((start_position, start_direction));

    while !queue.is_empty() {
        let (mut position, direction) = queue.pop_front().unwrap();

        let current_char = map.get(&position);
        let current_position = position.clone();
        let current_direction = direction.clone();
        let new_elements = match (&direction, current_char) {
            (_, '.') => {
                let (x, y) = direction.direction_offset();
                position.add(x, y);
                vec![(position, direction)]
            }
            (Direction::Bottom | Direction::Top, '|') => {
                let (x, y) = direction.direction_offset();
                position.add(x, y);
                vec![(position, direction)]
            }
            (Direction::Left | Direction::Right, '|') => {
                let mut position1 = position.clone();
                position1.add(0, 1);
                position.add(0, -1);
                vec![(position1, Direction::Bottom), (position, Direction::Top)]
            }
            (Direction::Left | Direction::Right, '-') => {
                let (x, y) = direction.direction_offset();
                position.add(x, y);
                vec![(position, direction)]
            }
            (Direction::Top | Direction::Bottom, '-') => {
                let mut position1 = position.clone();
                position1.add(-1, 0);
                vec![(position1, Direction::Left), (position, Direction::Right)]
            }
            (_, '/') => {
                let (offset, direction) = match direction {
                    Direction::Top => ((1, 0), Direction::Right),
                    Direction::Bottom => ((-1, 0), Direction::Left),
                    Direction::Left => ((0, 1), Direction::Bottom),
                    Direction::Right => ((0, -1), Direction::Top),
                };
                position.add(offset.0, offset.1);
                vec![(position, direction)]
            }
            (_, '\\') => {
                let (offset, direction) = match direction {
                    Direction::Top => ((-1, 0), Direction::Left),
                    Direction::Bottom => ((1, 0), Direction::Right),
                    Direction::Left => ((0, -1), Direction::Top),
                    Direction::Right => ((0, 1), Direction::Bottom),
                };
                position.add(offset.0, offset.1);
                vec![(position, direction)]
            }
            _ => {
                unreachable!()
            }
        };

        for (new_position, new_direction) in new_elements {
            if map.is_valid_position(&new_position)
                && !visited.contains(&(new_position.clone(), new_direction.clone()))
            {
                queue.push_back((new_position, new_direction));
            }
        }

        visited.insert((current_position, current_direction));
    }

    visited
        .iter()
        .fold(HashSet::<Position>::new(), |mut acc, (position, _)| {
            acc.insert(position.clone());
            acc
        })
        .len()
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn test_part1() {
        let input = include_str!("../../input/examples/16.txt");

        assert_eq!(part1(input), 46);
    }

    #[test]
    fn test_part2() {
        let input = include_str!("../../input/examples/16.txt");

        assert_eq!(part2(input), 51);
    }
}
