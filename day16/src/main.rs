use std::fs::read_to_string;

use day16::{part1, part2};

use profile::execute_with_profile;

fn main() {
    // let input = read_to_string("input/16.txt").unwrap();
    let input = include_str!("../../input/16.txt");
    
    execute_with_profile!("Part 1", part1(&input));
    execute_with_profile!("Part 2", part2(&input));
}

