use itertools::Itertools;

struct MapSection {
    destination_range_start: u64,
    source_range_start: u64,
    range_length: u64,
}

struct Map {
    sections: Vec<MapSection>,
}

impl Map {
    fn from(map_description: &str) -> Self {
        let sections = map_description
            .lines()
            .skip(1)
            .map(|section_description| {
                let section_numbers = section_description
                    .split(' ')
                    .map(|number| number.parse::<u64>().unwrap())
                    .collect::<Vec<_>>();

                MapSection {
                    destination_range_start: section_numbers[0],
                    source_range_start: section_numbers[1],
                    range_length: section_numbers[2],
                }
            })
            .collect::<Vec<_>>();

        Map { sections }
    }

    fn get_destination_number(&self, source_number: u64) -> u64 {
        self.sections
            .iter()
            .find_map(|section| {
                if section.source_range_start <= source_number
                    && section.source_range_start + section.range_length > source_number
                {
                    Some(
                        section.destination_range_start + source_number
                            - section.source_range_start,
                    )
                } else {
                    None
                }
            })
            .unwrap_or(source_number)
    }
}

pub fn part1(input: &str) -> u64 {
    let parts = input.split("\n\n").collect::<Vec<_>>();

    let seed_to_soil_map = Map::from(parts[1]);
    let soil_to_fertilize_map = Map::from(parts[2]);
    let fertilizer_to_water_map = Map::from(parts[3]);
    let water_to_light_map = Map::from(parts[4]);
    let light_to_temperature_map = Map::from(parts[5]);
    let temperature_to_humidity_map = Map::from(parts[6]);
    let humidity_to_location_map = Map::from(parts[7]);

    let maps = [
        seed_to_soil_map,
        soil_to_fertilize_map,
        fertilizer_to_water_map,
        water_to_light_map,
        light_to_temperature_map,
        temperature_to_humidity_map,
        humidity_to_location_map,
    ];

    parts[0]
        .split_once(": ")
        .unwrap()
        .1
        .split(' ')
        .map(|number| number.parse::<u64>().unwrap())
        .fold(u64::MAX, |min_location, number| {
            let location = maps.iter().fold(number, |current_value, map| {
                map.get_destination_number(current_value)
            });
            if location < min_location {
                location
            } else {
                min_location
            }
        })
}

pub fn part2(input: &str) -> u64 {
    let parts = input.split("\n\n").collect::<Vec<_>>();

    let seed_to_soil_map = Map::from(parts[1]);
    let soil_to_fertilize_map = Map::from(parts[2]);
    let fertilizer_to_water_map = Map::from(parts[3]);
    let water_to_light_map = Map::from(parts[4]);
    let light_to_temperature_map = Map::from(parts[5]);
    let temperature_to_humidity_map = Map::from(parts[6]);
    let humidity_to_location_map = Map::from(parts[7]);

    let maps = [
        seed_to_soil_map,
        soil_to_fertilize_map,
        fertilizer_to_water_map,
        water_to_light_map,
        light_to_temperature_map,
        temperature_to_humidity_map,
        humidity_to_location_map,
    ];

    let tuples = parts[0]
        .split_once(": ")
        .unwrap()
        .1
        .split(' ')
        .map(|number| number.parse::<u64>().unwrap())
        .tuples();

    let mut min_location = u64::MAX;
    for (start, length) in tuples {
        for value in start..start + length {
            let location = maps.iter().fold(value, |current_value, map| {
                map.get_destination_number(current_value)
            });
            if location < min_location {
                min_location = location
            }
        }
    }

    min_location
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn test_part1() {
        let input = include_str!("../../input/examples/5.txt");

        assert_eq!(part1(input), 35);
    }

    #[test]
    fn test_part2() {
        let input = include_str!("../../input/examples/5.txt");

        assert_eq!(part2(input), 46);
    }
}
