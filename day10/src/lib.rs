use std::collections::HashSet;

pub fn part1(input: &str) -> u32 {
    let mut map = input
        .lines()
        .map(|line| line.chars().collect::<Vec<_>>())
        .collect::<Vec<_>>();

    let starting_position = map
        .iter()
        .enumerate()
        .find_map(|(y, line)| {
            line.iter().enumerate().find_map(
                |(x, char)| {
                    if *char == 'S' {
                        Some((x, y))
                    } else {
                        None
                    }
                },
            )
        })
        .unwrap();

    replace_starting_symbol(&mut map, starting_position);

    let mut previous_position = starting_position;
    let mut current_position = get_next_positions(
        map[starting_position.1][starting_position.0],
        starting_position,
    )[0];

    let mut counter = 1;
    while current_position != starting_position {
        let current_x = current_position.0;
        let current_y = current_position.1;
        let current_character = map[current_y][current_x];
        let possible_next_positions = get_next_positions(current_character, current_position);
        let next_position = possible_next_positions
            .into_iter()
            .find(|pos| *pos != previous_position)
            .unwrap();
        previous_position = current_position;
        current_position = next_position;
        counter += 1;
    }

    counter / 2
}

fn get_next_positions(
    current_character: char,
    current_position: (usize, usize),
) -> Vec<(usize, usize)> {
    let current_x = current_position.0;
    let current_y = current_position.1;
    match current_character {
        '|' => vec![(current_x, current_y - 1), (current_x, current_y + 1)],
        '-' => vec![(current_x - 1, current_y), (current_x + 1, current_y)],
        'L' => vec![(current_x, current_y - 1), (current_x + 1, current_y)],
        'J' => vec![(current_x, current_y - 1), (current_x - 1, current_y)],
        '7' => vec![(current_x - 1, current_y), (current_x, current_y + 1)],
        'F' => vec![(current_x + 1, current_y), (current_x, current_y + 1)],

        _ => unreachable!(),
    }
}

fn replace_starting_symbol(map: &mut Vec<Vec<char>>, starting_position: (usize, usize)) {
    if is_vertical_pipe(map, starting_position) {
        map[starting_position.1][starting_position.0] = '|';
    } else if is_horizontal_pipe(map, starting_position) {
        map[starting_position.1][starting_position.0] = '-';
    } else if is_l_pipe(map, starting_position) {
        map[starting_position.1][starting_position.0] = 'L';
    } else if is_j_pipe(map, starting_position) {
        map[starting_position.1][starting_position.0] = 'J';
    } else if is_7_pipe(map, starting_position) {
        map[starting_position.1][starting_position.0] = '7';
    } else if is_f_pipe(map, starting_position) {
        map[starting_position.1][starting_position.0] = 'F';
    }
}

fn is_vertical_pipe(map: &Vec<Vec<char>>, starting_position: (usize, usize)) -> bool {
    let start_x = starting_position.0 as i32;
    let start_y = starting_position.1 as i32;

    let max_y = map.len() as i32;

    start_y - 1 > 0
        && ['|', '7', 'F'].contains(&get_character(map, start_x, start_y - 1))
        && start_y + 1 < max_y
        && ['|', 'L', 'J'].contains(&get_character(map, start_x, start_y + 1))
}

fn is_horizontal_pipe(map: &Vec<Vec<char>>, starting_position: (usize, usize)) -> bool {
    let start_x = starting_position.0 as i32;
    let start_y = starting_position.1 as i32;

    let max_x = map[0].len() as i32;

    start_x - 1 > 0
        && ['F', 'L', '-'].contains(&get_character(map, start_x - 1, start_y))
        && start_x + 1 < max_x
        && ['7', 'J', '-'].contains(&get_character(map, start_x + 1, start_y))
}

fn is_l_pipe(map: &Vec<Vec<char>>, starting_position: (usize, usize)) -> bool {
    let start_x = starting_position.0 as i32;
    let start_y = starting_position.1 as i32;

    let max_x = map[0].len() as i32;

    start_y - 1 > 0
        && ['|', 'F', '7'].contains(&get_character(map, start_x, start_y - 1))
        && start_x + 1 < max_x
        && ['7', 'J', '-'].contains(&get_character(map, start_x + 1, start_y))
}

fn is_j_pipe(map: &Vec<Vec<char>>, starting_position: (usize, usize)) -> bool {
    let start_x = starting_position.0 as i32;
    let start_y = starting_position.1 as i32;

    start_y - 1 > 0
        && ['|', 'F', '7'].contains(&get_character(map, start_x, start_y - 1))
        && start_x - 1 > 0
        && ['L', 'F', '-'].contains(&get_character(map, start_x - 1, start_y))
}

fn is_7_pipe(map: &Vec<Vec<char>>, starting_position: (usize, usize)) -> bool {
    let start_x = starting_position.0 as i32;
    let start_y = starting_position.1 as i32;

    let max_y = map.len() as i32;

    start_y + 1 < max_y
        && ['|', 'L', 'J'].contains(&get_character(map, start_x, start_y + 1))
        && start_x - 1 > 0
        && ['L', 'F', '-'].contains(&get_character(map, start_x - 1, start_y))
}

fn is_f_pipe(map: &Vec<Vec<char>>, starting_position: (usize, usize)) -> bool {
    let start_x = starting_position.0 as i32;
    let start_y = starting_position.1 as i32;

    let max_y = map.len() as i32;
    let max_x = map[0].len() as i32;

    start_y + 1 < max_y
        && ['|', 'L', 'J'].contains(&get_character(map, start_x, start_y + 1))
        && start_x + 1 < max_x
        && ['7', 'J', '-'].contains(&get_character(map, start_x + 1, start_y))
}

fn get_character(map: &Vec<Vec<char>>, x: i32, y: i32) -> char {
    map[y as usize][x as usize]
}

pub fn part2(input: &str) -> u32 {
    let mut map = input
        .lines()
        .map(|line| line.chars().collect::<Vec<_>>())
        .collect::<Vec<_>>();

    let starting_position = map
        .iter()
        .enumerate()
        .find_map(|(y, line)| {
            line.iter().enumerate().find_map(
                |(x, char)| {
                    if *char == 'S' {
                        Some((x, y))
                    } else {
                        None
                    }
                },
            )
        })
        .unwrap();

    replace_starting_symbol(&mut map, starting_position);

    let mut previous_position = starting_position;
    let mut current_position = get_next_positions(
        map[starting_position.1][starting_position.0],
        starting_position,
    )[0];

    let mut visited = HashSet::new();
    visited.insert(previous_position);

    while current_position != starting_position {
        visited.insert(current_position);
        let current_x = current_position.0;
        let current_y = current_position.1;
        let current_character = map[current_y][current_x];
        let possible_next_positions = get_next_positions(current_character, current_position);
        let next_position = possible_next_positions
            .into_iter()
            .find(|pos| *pos != previous_position)
            .unwrap();


        previous_position = current_position;
        current_position = next_position;
    }
    let mut counter = 0;
    let mut is_inside = false;

    for y in 0..map.len() {
        for x in 0..map[y].len() {
            if visited.contains(&(x, y)) {
                let character = map[y][x];
                if ['|', '7', 'F'].contains(&character) {
                    is_inside = !is_inside;
                }
            } else if is_inside {
                counter += 1;
            }
        }
    }
    counter
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn test_part1() {
        let input = ".....
.S-7.
.|.|.
.L-J.
.....";

        assert_eq!(part1(input), 4);

        let input = "..F7.
.FJ|.
SJ.L7
|F--J
LJ...";

        assert_eq!(part1(input), 8);
    }

    #[test]
    fn test_part2() {
        let input = "...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
...........";

        assert_eq!(part2(input), 4);

        let input = ".F----7F7F7F7F-7....
.|F--7||||||||FJ....
.||.FJ||||||||L7....
FJL7L7LJLJ||LJ.L-7..
L--J.L7...LJS7F-7L7.
....F-J..F7FJ|L7L7L7
....L7.F7||L7|.L7L7|
.....|FJLJ|FJ|F7|.LJ
....FJL-7.||.||||...
....L---J.LJ.LJLJ...";
        
        assert_eq!(part2(input), 8); 
    }
}
