use criterion::{criterion_group, criterion_main, Criterion, black_box};
use day2::{part1, part2};

pub fn criterion_benchmark(c: &mut Criterion) {
    let input = include_str!("../../input/2.txt");
    c.bench_function("day2-part1", |b| b.iter(|| part1(black_box(input))));
    c.bench_function("day2-part2", |b| b.iter(|| part2(black_box(input))));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
