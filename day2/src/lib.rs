pub fn part1(input: &str) -> u32 {
    let max_red_number = 12;
    let max_green_number = 13;
    let max_blue_number = 14;

    input.lines()
        .map(|line| {
            let (game, combinations) = line.split_once(": ").unwrap();

            for combination in combinations.split("; ") {

                for cube in combination.split(", ") {
                    let (number, color) = cube.split_once(' ').unwrap();

                    if color == "red" && number.parse::<i32>().unwrap() > max_red_number {
                        return 0;
                    }

                    if color == "green" && number.parse::<i32>().unwrap() >max_green_number {
                        return 0;
                    }

                    if color == "blue" && number.parse::<i32>().unwrap() > max_blue_number {
                        return 0;
                    }
                }
            }
            game.split_once(' ').unwrap().1.parse().unwrap()
        })
        .sum()
}

pub fn part2(input: &str) -> u32 {
    input.lines()
        .map(|line| {
            let mut max_red_number = 0;
            let mut max_green_number = 0;
            let mut max_blue_number = 0;

            let (_game, combinations) = line.split_once(": ").unwrap();

            for combination in combinations.split("; ") {

                for cube in combination.split(", ") {
                    let (number, color) = cube.split_once(' ').unwrap();

                    let number = number.parse::<u32>().unwrap();
                    if color == "red" {
                        max_red_number = max_red_number.max(number)
                    }

                    if color == "green" {
                        max_green_number = max_green_number.max(number);
                    }

                    if color == "blue" {
                        max_blue_number = max_blue_number.max(number);
                    }
                }
            }
            max_red_number * max_green_number * max_blue_number
        })
        .sum()
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn test_part1() {
        let input = include_str!("../../input/examples/2.txt");

        assert_eq!(part1(input), 8);
    }

    #[test]
    fn test_part2() {
        let input = include_str!("../../input/examples/2.txt");

        assert_eq!(part2(input), 2286);
    }
}
