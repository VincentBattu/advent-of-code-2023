pub fn part1(input: &str) -> i32 {
    input
        .lines()
        .map(|line| {
            let history_values = line.split(' ')
                .map(|value| value.parse::<i32>().unwrap())
                .collect::<Vec<_>>();

            let mut steps_histories_values = vec![history_values];

            while !steps_histories_values.last().unwrap().iter().all(|value| *value == 0) {
                steps_histories_values.push(steps_histories_values
                    .last()
                    .unwrap()
                    .windows(2)
                    .map(|elements| elements[1] - elements[0])
                    .collect()
                )
            }

            let len = steps_histories_values.len();
            steps_histories_values[len - 1].push(0);

            for i in (0..len - 1).rev() {
                let current_step_len = steps_histories_values[i].len();
                let current_step_previous_value = steps_histories_values[i][current_step_len - 1];

                let next_step_len =  steps_histories_values[i + 1].len();
                let next_step_previous_value = steps_histories_values[i + 1][next_step_len - 1];

                steps_histories_values[i].push(current_step_previous_value + next_step_previous_value)
            }
            *steps_histories_values.first().unwrap().last().unwrap()
        })
        .sum()
}

pub fn part2(input: &str) -> i32 {
    input
        .lines()
        .map(|line| {
            let history_values = line.split(' ')
                .map(|value| value.parse::<i32>().unwrap())
                .collect::<Vec<_>>();

            let mut steps_histories_values = vec![history_values];

            while !steps_histories_values.last().unwrap().iter().all(|value| *value == 0) {
                steps_histories_values.push(steps_histories_values
                    .last()
                    .unwrap()
                    .windows(2)
                    .map(|elements| elements[1] - elements[0])
                    .collect()
                )
            }

            let len = steps_histories_values.len();
            steps_histories_values[len - 1].insert(0, 0);

            for i in (0..len - 1).rev() {
                let current_step_previous_value = steps_histories_values[i][0];

                let next_step_previous_value = steps_histories_values[i + 1][0];

                steps_histories_values[i].insert(0, current_step_previous_value - next_step_previous_value)
            }
            *steps_histories_values.first().unwrap().first().unwrap()
        })
        .sum()
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn test_part1() {
        let input = include_str!("../../input/examples/9.txt");

        assert_eq!(part1(input), 114);
    }

    #[test]
    fn test_part2() {
        let input = include_str!("../../input/examples/9.txt");

        assert_eq!(part2(input), 2);
    }
}
