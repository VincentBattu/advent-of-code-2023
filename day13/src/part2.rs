pub fn part2 (input: &str) -> u32 {   input
    .split("\n\n")
    .map(|map_lines| {
        let map = parse_map(map_lines);

        if let Some(horizontal_line_index) = find_horizontal_reflection_line(&map) {
            return (horizontal_line_index + 1) * 100;
        }
        if let Some(vertical_line_index) = find_vertical_reflection_line(&map) {
            return vertical_line_index + 1;
        }
        unreachable!()
    })
    .sum()
}

fn parse_map(input: &str) -> Vec<Vec<char>> {
    input
        .lines()
        .map(|line| line.chars().collect())
        .collect()
}


fn find_horizontal_reflection_line (map: &[Vec<char>]) -> Option<u32> {
    let mut artefact_found = false;
    if rows_equal(map, 0, 1, &mut artefact_found) && artefact_found{
        return Some(0);
    }

    artefact_found = false;
    if rows_equal(map, map.len() - 2, map.len() - 1, &mut artefact_found)  && artefact_found{
        return Some((map.len() as u32) - 2);
    }

    for line_index in 1..map.len() - 1 {
        artefact_found = false;
        let steps_top = line_index + 1;
        let steps_bottom = map.len() - line_index - 1;

        let mut line_found = true;
        for y in 0..steps_top.min(steps_bottom) {
            if !rows_equal(map, line_index - y, line_index + y + 1, &mut artefact_found) {
                line_found = false;
                break;
            }
        }
        if line_found && artefact_found{
            return Some(line_index as u32);
        }
    }
    None
}

fn find_vertical_reflection_line (map: &[Vec<char>]) ->  Option<u32>{
    let mut artefact_found = false;

    if columns_equal(map, 0, 1, &mut artefact_found) && artefact_found {
        return Some(0);
    }

    artefact_found = false;
    if columns_equal(map, map[0].len() - 2, map[0].len() - 1,  &mut artefact_found) && artefact_found {
        return Some(map[0].len() as u32 - 2);
    }

    for column_index in 1..map[0].len() - 1 {
        artefact_found = false;
        let steps_left = column_index + 1;
        let steps_right = map[0].len() - column_index - 1;

        let mut line_found = true;
        for x in 0..steps_left.min(steps_right) {
            if !columns_equal(map, column_index - x, column_index + x + 1,  &mut artefact_found) {
                line_found = false;
                break;
            }
        }
        if line_found && artefact_found {
            return Some(column_index as u32);
        }
    }
    None
}

fn rows_equal (map: &[Vec<char>], row_index_1: usize, row_index_2: usize, artefact_found: &mut bool) -> bool {
    let number_of_equals = map[row_index_1]
        .iter()
        .zip(map[row_index_2].iter())
        .filter(|(char1, char2)| *char1 == *char2)
        .count();

    if number_of_equals == map[0].len() {
      true
    } else if number_of_equals == map[0].len() - 1 && !*artefact_found {
      *artefact_found = true;
      true
    } else {
      false
    }
}

fn columns_equal (map: &[Vec<char>], column_index_1: usize, column_index_2: usize, artefact_found: &mut bool) -> bool {
    let number_of_equals = map
        .iter()
        .zip(map.iter())
        .filter(|(line1, line2)| line1[column_index_1] == line2[column_index_2])
        .count();

    if number_of_equals == map.len() {
        true
    } else if number_of_equals == map.len() - 1 && !*artefact_found {
        *artefact_found = true;
        true
    } else {
        false
    }
}


#[cfg(test)]
mod test {
    use crate::part2::*;

    #[test]
    fn test() {
        let input = "#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#";

    assert_eq!(part2(input), 400);
    }
}