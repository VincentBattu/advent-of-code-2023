use std::collections::HashSet;

pub fn part1(input: &str) -> i32 {
    input.lines()
        .map(|line| {           
            let actual_winning_numbers_count = count_actual_winning_numbers(line) as i32;
            
            if actual_winning_numbers_count == 0 {
                0
            } else {
                i32::pow(2, (actual_winning_numbers_count - 1).max(0) as u32)
            }
        })
        .sum()
}


pub fn part2(input: &str) -> u32 {
    let lines = input.lines()
        .enumerate()
        .collect::<Vec<_>>();
    let mut cards_count = vec![1; lines.len()];

    lines
        .into_iter()
        .for_each(|(i, line)| {
            let actual_winning_numbers_count = count_actual_winning_numbers(line);

            for j in i + 1..i + 1 + actual_winning_numbers_count {
                cards_count[j] += cards_count[i];
            }
        });
    
    cards_count.iter().sum()
}

fn count_actual_winning_numbers (line: &str) -> usize {
    let (_, numbers_lists) = line.split_once(": ").unwrap();
    let (winning_numbers, actual_numbers) = numbers_lists.split_once(" | ").unwrap();

    let winning_numbers = winning_numbers
        .split(' ')
        .filter(|number| !number.eq(&String::from("")))
        .map(|number| number.parse::<u32>().unwrap())
        .collect::<HashSet<_>>();

    let actual_numbers = actual_numbers
        .split(' ')
        .filter(|number| !number.eq(&String::from("")))
        .map(|number| number.parse::<u32>().unwrap())
        .collect::<Vec<_>>();
    
    actual_numbers.into_iter()
        .filter(|actual_number| winning_numbers.contains(actual_number))
        .count()
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn test_part1() {
        let input = include_str!("../../input/examples/4.txt");

        assert_eq!(part1(input), 13);
    }

    #[test]
    fn test_part2() {
        let input = include_str!("../../input/examples/4.txt");

        assert_eq!(part2(input), 30);
    }
}
