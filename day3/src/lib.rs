use std::collections::HashMap;

pub fn part1(input: &str) -> u32 {
    let characters = input
        .lines()
        .map(|line| line.chars().collect::<Vec<_>>())
        .collect::<Vec<_>>();

    let mut sum = 0;
    for y in 0..characters.len() {
        let mut parsing_number = false;
        let mut start_number = 0;
        let mut end_number = 0;
        for x in 0..characters[y].len() {
            let character = characters[y][x];
            if character.is_ascii_digit() {
                if !parsing_number {
                    start_number = x;
                    parsing_number = true;
                }
                end_number = x;
            }

            if (!character.is_ascii_digit() && parsing_number)
                || (character.is_ascii_digit() && parsing_number && x == characters[y].len() - 1)
            {
                parsing_number = false;
                if is_adjacent_to_symbol(&characters, start_number, end_number + 1, y) {
                    let number = characters[y][start_number..=end_number]
                        .iter()
                        .collect::<String>()
                        .parse::<u32>()
                        .unwrap();

                    sum += number;
                }
            }
        }
    }
    sum
}

fn is_adjacent_to_symbol(
    characters: &Vec<Vec<char>>,
    start_x: usize,
    end_x: usize,
    y: usize,
) -> bool {
    let min_boudary_x = (start_x as i32 - 1).max(0) as usize;
    let max_boundary_x = end_x.min(characters[0].len() - 1);
    let min_boundary_y = (y as i32 - 1).max(0) as usize;
    let max_boundary_y = (y + 1).min(characters.len() - 1);

    characters[min_boundary_y..=max_boundary_y]
        .iter()
        .any(|line| {
            line[min_boudary_x..=max_boundary_x]
                .iter()
                .any(|character| *character != '.' && !character.is_ascii_digit())
        })
}

pub fn part2(input: &str) -> u32 {
    let characters = input
        .lines()
        .map(|line| line.chars().collect::<Vec<_>>())
        .collect::<Vec<_>>();

    let mut adjacent_star_numbers: HashMap<(usize, usize), Vec<u32>> = HashMap::new();

    for y in 0..characters.len() {
        let mut parsing_number = false;
        let mut start_number = 0;
        let mut end_number = 0;
        for x in 0..characters[y].len() {
            let character = characters[y][x];
            if character.is_ascii_digit() {
                if !parsing_number {
                    start_number = x;
                    parsing_number = true;
                }
                end_number = x;
            }

            if (!character.is_ascii_digit() && parsing_number)
                || (character.is_ascii_digit() && parsing_number && x == characters[y].len() - 1)
            {
                parsing_number = false;
                if let Some(star_adjacent_position) = get_star_adjacent_position(&characters, start_number, end_number + 1, y) {
                    let number = characters[y][start_number..=end_number]
                        .iter()
                        .collect::<String>()
                        .parse::<u32>()
                        .unwrap();

                    let numbers  = adjacent_star_numbers.entry(star_adjacent_position).or_default();
                    numbers.push(number);
                }
            }
        }
    }

    adjacent_star_numbers
        .values()
        .filter(|numbers| numbers.len()== 2)
        .map(|numbers| numbers.iter().product::<u32>())
        .sum()
}

fn get_star_adjacent_position(
    characters: &Vec<Vec<char>>,
    start_x: usize,
    end_x: usize,
    y: usize,
) -> Option<(usize, usize)> {
    let min_boudary_x = (start_x as i32 - 1).max(0) as usize;
    let max_boundary_x = end_x.min(characters[0].len() - 1);
    let min_boundary_y = (y as i32 - 1).max(0) as usize;
    let max_boundary_y = (y + 1).min(characters.len() - 1);

   for y in min_boundary_y..=max_boundary_y {
    let line = &characters[y];
        for x in min_boudary_x..=max_boundary_x {
            let character = line[x];
            if character == '*' {
                return Some((x, y));
            }
        }
   }

   None
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn test_part1() {
        let input = include_str!("../../input/examples/3.txt");

        assert_eq!(part1(input), 4361);
    }

    #[test]
    fn test_part2() {
        let input = include_str!("../../input/examples/3.txt");

        assert_eq!(part2(input), 467835);
    }
}
