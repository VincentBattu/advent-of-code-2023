use std::{collections::HashMap, cmp::Ordering};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
enum Type {
    FiveOfAKind,
    FourOfAKind,
    FullHouse,
    ThreeOfAKind,
    TwoPair,
    OnePair,
    HighCard
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
enum Card {
    J,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    T,
    Q,
    K,
    A
}

impl From<char> for Card {
    fn from(value: char) -> Self {
        match value {
            '2' => Card::Two,
            '3' => Card::Three,
            '4' => Card::Four,
            '5' => Card::Five,
            '6' => Card::Six,
            '7' => Card::Seven,
            '8' => Card::Eight,
            '9' => Card::Nine,
            'T' => Card::T,
            'J' => Card::J,
            'Q' => Card::Q,
            'K' => Card::K,
            'A' => Card::A,
            _ => unreachable!()
        }
    }
}

#[derive(Debug)]
struct Hand {
    cards: Vec<Card>,
    type_of_hand: Type,
    bid: u64
}


pub fn part2(input: &str) -> u64 {
    let mut hands = input.lines()
        .map(|line| {
            let (hand, bid) = line.split_once(' ').unwrap();

            let (mut counts, cards) = hand.chars()
                .fold((HashMap::<char, u8>::with_capacity(13), Vec::with_capacity(5)), |(mut counts, mut cards), card_character| {
                    let count = counts.entry(card_character).or_insert(0);
                    *count += 1;

                    cards.push(Card::from(card_character));
                    
                    (counts, cards)
                });
            
            let bid = bid.parse::<u64>().unwrap();

            let number_of_jokers = if counts.contains_key(&'J') {
                let number = counts.get(&'J').unwrap();
                *number
            } else {
                0
            };
            counts.remove(&'J');

            let mut sorted_counts = counts.into_iter()
                .collect::<Vec<_>>();
            sorted_counts.sort_by(|card_count_1, card_count_2| card_count_2.1.cmp(&card_count_1.1));

            if number_of_jokers == 5 {
                sorted_counts.push(('A', 5));
            } else {
                sorted_counts[0].1 += number_of_jokers;
            }


            if sorted_counts.len() == 1 {
                return Hand { type_of_hand: Type::FiveOfAKind, cards, bid }
            }
            
            if sorted_counts.len() == 5 {
                return Hand { type_of_hand: Type::HighCard, cards, bid }
            }


            if sorted_counts[0].1 == 4 {
                Hand { type_of_hand: Type::FourOfAKind, cards, bid }
            } else if sorted_counts[0].1 == 3 && sorted_counts[1].1 == 2 {
                Hand { type_of_hand: Type::FullHouse, cards, bid }
            } else if sorted_counts[0].1 == 3 {
                Hand { type_of_hand: Type::ThreeOfAKind, cards, bid }
            } else if sorted_counts[0].1 == 2 && sorted_counts[1].1 == 2 {
                Hand { type_of_hand: Type::TwoPair, cards, bid }
            } else {
                Hand { type_of_hand: Type::OnePair, cards, bid }
            }
        })
        .collect::<Vec<_>>();

        hands.sort_by(|hand1, hand2| {
           let type_of_hand_comparison = hand2.type_of_hand.cmp(&hand1.type_of_hand);
           if type_of_hand_comparison != Ordering::Equal {
                type_of_hand_comparison
           } else {
                for (card1, card2) in hand1.cards.iter().zip(&hand2.cards) {
                    let comparison = card1.cmp(card2);
                    if comparison != Ordering::Equal {
                        return comparison;
                    }
                }
                Ordering::Equal
           }
        });

        hands.
            iter()
            .enumerate()
            .map(|(index, hand)| {
                (index as u64 + 1) * hand.bid
            })
            .sum()
}

#[cfg(test)]
mod test {
    use crate::part2::part2;

    #[test]
    fn test_part2() {
        let input = include_str!("../../input/examples/7.txt");

        assert_eq!(part2(input), 5905);
    }
}


