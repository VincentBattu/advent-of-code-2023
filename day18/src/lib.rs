#[derive(Hash, PartialEq, Eq, Clone, Copy, Debug)]
struct Position {
    x: i64,
    y: i64,
}

struct Instruction<'a> {
    direction: &'a str,
    number_of_steps: u64,
}

pub fn part1(input: &str) -> u64 {
    let instructions = input
        .lines()
        .map(|line| {
            let split = line.split(' ').collect::<Vec<_>>();

            let direction = split[0];
            let number_of_steps = split[1].parse::<u64>().unwrap();

            Instruction {
                direction,
                number_of_steps,
            }
        })
        .collect::<Vec<_>>();

    solve(&instructions)
}

pub fn part2(input: &str) -> u64 {
    let instructions = input
        .lines()
        .map(|line| {
            let split = line.split(' ').collect::<Vec<_>>();

            let hexa_code = split[2].strip_prefix("(#").unwrap().strip_suffix(')').unwrap();

            Instruction {
                direction: match &hexa_code[5..] {
                    "0" => "R",
                    "1" => "D",
                    "2" => "L",
                    "3" => "U",
                    _ => unreachable!()
                },
                number_of_steps: u64::from_str_radix(&hexa_code[0..5], 16).unwrap(),
            }
        })
        .collect::<Vec<_>>();

    solve(&instructions)
}

fn solve(instructions: &[Instruction]) -> u64 {
    let mut current_position = Position { x: 0, y: 0 };

    let mut vertices = Vec::<Position>::new();

    let mut perimeter = 0u64;
    instructions.iter().for_each(|instruction| {
        let number_of_steps = instruction.number_of_steps as i64;
        let new_position = match instruction.direction {
            "U" => Position {
                x: current_position.x,
                y: current_position.y - number_of_steps,
            },
            "D" => Position {
                x: current_position.x,
                y: current_position.y + number_of_steps,
            },
            "L" => Position {
                x: current_position.x - number_of_steps,
                y: current_position.y,
            },
            "R" => Position {
                x: current_position.x + number_of_steps,
                y: current_position.y,
            },
            _ => unreachable!(),
        };

        perimeter += number_of_steps as u64;

        vertices.push(new_position);
        current_position = new_position;
    });
    // Add half of the perimeter since get_area already takes into account "half" of the thick perimeter line
    get_area(&vertices) + perimeter / 2 + 1
}

/**
 * https://www.mathopenref.com/coordpolygonarea.html
 */
fn get_area(vertices: &[Position]) -> u64 {
    let mut vertices = vertices.to_vec();
    vertices.push(vertices[0]);
    let sum = vertices.windows(2).fold(0i64, |mut sum, points: &[Position]| {
        sum += points[0].x * points[1].y - points[0].y * points[1].x;
        sum
    });

    (sum / 2).unsigned_abs()
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn test_part1() {
        let input = "R 6 (#70c710)
D 5 (#0dc571)
L 2 (#5713f0)
D 2 (#d2c081)
R 2 (#59c680)
D 2 (#411b91)
L 5 (#8ceee2)
U 2 (#caa173)
L 1 (#1b58a2)
U 2 (#caa171)
R 2 (#7807d2)
U 3 (#a77fa3)
L 2 (#015232)
U 2 (#7a21e3)";

        assert_eq!(part1(input), 62);
    }

    #[test]
    fn test_part2() {
        let input = "R 6 (#70c710)
D 5 (#0dc571)
L 2 (#5713f0)
D 2 (#d2c081)
R 2 (#59c680)
D 2 (#411b91)
L 5 (#8ceee2)
U 2 (#caa173)
L 1 (#1b58a2)
U 2 (#caa171)
R 2 (#7807d2)
U 3 (#a77fa3)
L 2 (#015232)
U 2 (#7a21e3)";

        assert_eq!(part2(input), 952408144115);
    }
}
