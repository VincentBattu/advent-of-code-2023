use std::fs::read_to_string;

use day18::{part1, part2};

use profile::execute_with_profile;

fn main() {
    let input = read_to_string("input/18.txt").unwrap();

    execute_with_profile!("Part 1", part1(&input));
    execute_with_profile!("Part 2", part2(&input));
}
