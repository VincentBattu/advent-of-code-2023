use std::collections::HashMap;

#[derive(Debug)]
struct Destination<'a>(&'a str, &'a str);

pub fn part1(input: &str) -> usize {
    let (map, steps) = parse(input);

    walk("AAA", &map, &steps, "ZZZ")
}

fn walk (start_position: &str, map: &HashMap<&str, Destination<'_>>, steps: &Vec<char>, ends_with: &str) -> usize {
    let mut position = start_position;
    let mut number_of_steps = 0;
    while !position.ends_with(ends_with) {
        let step = steps[number_of_steps % steps.len()];

        position = match step {
            'L' => map.get(position).unwrap().0,
            'R' => map.get(position).unwrap().1,
            _ => unreachable!(),
        };

        number_of_steps += 1;
    }
    number_of_steps
}

pub fn part2(input: &str) -> usize {
    let (map, steps) = parse(input);

    let numbers_of_steps = map
        .keys()
        .filter(|key| key.ends_with('A'))
        .map(|key| walk(key, &map, &steps, "Z"))
        .collect::<Vec<_>>();

    lcm(&numbers_of_steps)
}

fn parse(input: &str) -> (HashMap<&str, Destination>, Vec<char>) {
    let mut lines = input.lines();

    let steps = lines.next().unwrap().chars().collect::<Vec<_>>();

    let map = lines
        .skip(1)
        .fold(HashMap::<&str, Destination>::new(), |mut map, line| {
            let (from, to) = line.split_once(" = ").unwrap();
            let (left, right) = &to[1..to.len() - 1].split_once(", ").unwrap();

            map.insert(from, Destination(left, right));

            map
        });

    (map, steps)
}

pub fn lcm(nums: &[usize]) -> usize {
    if nums.len() == 1 {
        return nums[0];
    }
    let a = nums[0];
    let b = lcm(&nums[1..]);
    a * b / gcd_of_two_numbers(a, b)
}

fn gcd_of_two_numbers(a: usize, b: usize) -> usize {
    if b == 0 {
        return a;
    }
    gcd_of_two_numbers(b, a % b)
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn test_part1() {
        let input = "RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)";

        assert_eq!(part1(input), 2);

        let input = "LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)";

        assert_eq!(part1(input), 6);
    }

    #[test]
    fn test_part2() {
        let input = "LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)";

        assert_eq!(part2(input), 6);
    }
}
