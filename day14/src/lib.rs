use std::{
    collections::HashMap,
    hash::{Hash, Hasher},
};

#[derive(PartialEq, Eq, Clone)]
struct Map {
    height: usize,
    width: usize,
    data: Vec<Vec<char>>,
}

impl Map {
    fn from(input: &str) -> Self {
        let data = input
            .lines()
            .map(|line| line.chars().collect())
            .collect::<Vec<Vec<_>>>();
        let height = data.len();
        let width = data[0].len();
        Self {
            data,
            height,
            width,
        }
    }

    fn tilt_top(&mut self) {
        for y in 1..self.data.len() {
            for x in 0..self.data[y].len() {
                if self.data[y][x] == 'O' {
                    let first_empty_north_position = (0..=y - 1).rev().find_map(|y| {
                        if y == 0 && self.data[y][x] == '.' {
                            Some(y)
                        } else if ['#', 'O'].contains(&self.data[y][x]) {
                            Some(y + 1)
                        } else {
                            None
                        }
                    });

                    if let Some(first_not_empty_north_position) = first_empty_north_position {
                        self.data[y][x] = '.';
                        self.data[first_not_empty_north_position][x] = 'O';
                    }
                }
            }
        }
    }

    fn tilt_bottom(&mut self) {
        for y in (0..self.height - 1).rev() {
            for x in 0..self.width {
                if self.data[y][x] == 'O' {
                    let mut first_cube_rock_y_position = y + 1;
                    while first_cube_rock_y_position < self.height
                        && self.data[first_cube_rock_y_position][x] == '.'
                    {
                        first_cube_rock_y_position += 1;
                    }
                    self.data[y][x] = '.';
                    self.data[first_cube_rock_y_position - 1][x] = 'O';
                }
            }
        }
    }

    fn tilt_right(&mut self) {
        for x in (0..self.width - 1).rev() {
            for y in 0..self.height {
                if self.data[y][x] == 'O' {
                    let mut first_cube_rock_x_position = x + 1;
                    while first_cube_rock_x_position < self.width
                        && self.data[y][first_cube_rock_x_position] == '.'
                    {
                        first_cube_rock_x_position += 1;
                    }
                    self.data[y][x] = '.';
                    self.data[y][first_cube_rock_x_position - 1] = 'O';
                }
            }
        }
    }

    fn tilt_left(&mut self) {
        for x in 1..self.width {
            for y in 0..self.height {
                if self.data[y][x] == 'O' {
                    let mut first_cube_rock_x_position = x as i32 - 1;
                    while first_cube_rock_x_position >= 0
                        && self.data[y][first_cube_rock_x_position as usize] == '.'
                    {
                        first_cube_rock_x_position -= 1;
                    }
                    self.data[y][x] = '.';
                    self.data[y][(first_cube_rock_x_position + 1).max(0) as usize] = 'O';
                }
            }
        }
    }

    fn calculate_load(&self) -> u32 {
        self.data
            .iter()
            .enumerate()
            .map(|(i, line)| {
                let number_of_rounded_rock = line.iter().filter(|char| **char == 'O').count();
                (number_of_rounded_rock * (self.data.len() - i)) as u32
            })
            .sum()
    }
}

impl Hash for Map {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.height.hash(state);
        self.width.hash(state);
        self.data.hash(state);
    }
}

pub fn part1(input: &str) -> u32 {
    let mut map = Map::from(input);
    map.tilt_top();
    map.calculate_load()
}

pub fn part2(input: &str) -> u32 {
    let mut history = vec![Map::from(input)];

    let mut i = 0;
    loop {
        let mut current_map = history.last().unwrap().clone();
        let new_map = run_cycle(&mut current_map);

        if let Some((start_cycle, _)) = history
            .iter()
            .enumerate()
            .find(|(_, map)| **map == *new_map)
        {
            let remaining_step = 1_000_000_000 - start_cycle;
            let cycle_length = i - start_cycle + 1;

            let last_map = history[start_cycle + remaining_step % cycle_length].clone();
            return last_map.calculate_load();
        }
        history.push(new_map.clone());
        i += 1;
    }
}

fn run_cycle(map: &mut Map) -> &Map {
    map.tilt_top();
    map.tilt_left();
    map.tilt_bottom();
    map.tilt_right();
    map
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn test_part1() {
        let input = "O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#....";

        assert_eq!(part1(input), 136);
    }

    #[test]
    fn test_part2() {
        let input = "O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#....";

        assert_eq!(part2(input), 64);
    }
}
