#[macro_export]
macro_rules! execute_with_profile {
    ($prefix:expr, $result:expr) => {
        let start = std::time::Instant::now();
        let result = $result;
        let elapsed = start.elapsed();
        println!("{}: {result}. Executed in {elapsed:?}", $prefix);
    };
}
