import { argv, exit } from 'node:process'
import { writeFile, readFile } from 'node:fs/promises'
import { EOL } from 'node:os'

await injectFromDotEnv()

const { day, sessionCookie, year } = validateAndGetParameters()

const result = await fetch(`https://adventofcode.com/${year}/day/${day}/input`, {
  headers: {
    cookie: sessionCookie
  }
})
let body = await result.text()
body = body.replace(/(\n|\r\n)/g, EOL)

if (result.ok) {
  const dataPath = `input/${day}.txt`
  const examplePath = `input/examples/${day}.txt`
  await Promise.all([writeFile(dataPath, body.trim()), writeFile(examplePath, '')])
  console.log(`Day ${day} data successfully downloaded in ${dataPath}`)
  console.log(`Empty file created at ${examplePath}`)
} else {
  console.error('Your cookie seems to have expired, you shoud probably renew it')
  const responseDetail = {
    status: result.status,
    statusText: result.statusText,
    body
  }
  console.error({ responseDetail })
}


async function injectFromDotEnv() {
  let fileContent
  try {
    fileContent = await readFile('./.env', {
      encoding: 'utf8'
    })
  } catch (e) {
    return
  }

  for (const line of fileContent.split(EOL)) {
    const i = line.indexOf('=')
    process.env[line.slice(0, i)] = line.slice(i + 1)
  }

}

function validateAndGetParameters() {
  const day = validateAndGetDayNumber()
  const { sessionCookie, year } = validateAndGetSessionCookie()

  return { day, sessionCookie, year }
}

function validateAndGetDayNumber() {
  const day = argv[2]

  if (!day) {
    console.error('Missing day parameter. Consider adding the day number as a command parameter')
    console.error()
    console.error('Usage example:')
    console.error('node scripts/init.mjs 11')
    console.error('                      ^^')
    console.error('                      add the day number here')
    exit(1)
  }

  return day
}

function validateAndGetSessionCookie() {
  const sessionCookie = process.env.AOC_SESSION_COOKIE
  const year = process.env.YEAR

  if (!sessionCookie) {
    console.error('Missing session cookie. Please define "AOC_SESSION_COOKIE" env variable with the website cookie.value. .env file is supported')
    exit(1)
  }

  if (!year) {
    console.error('Missing year. Please define "YEAR" env variable with the current AOC year. .env file is supported')
    exit(1)
  }

  return { sessionCookie, year }
}